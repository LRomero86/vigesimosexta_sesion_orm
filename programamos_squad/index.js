const express = require('express');
const app = express();

//llamamos a la db

require('./db');

const apiRouter = require('./routes/api');

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/api', apiRouter);

app.listen(3000, () => {
    console.log('Conectado a servidor en port 3000');
});
