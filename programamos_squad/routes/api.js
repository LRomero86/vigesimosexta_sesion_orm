const router = require('Express').Router();
const apiTv = require('./api/tvs');

router.use('/tvs', apiTv);

module.exports = router;