const { Sequelize } = require('sequelize');
const tvModelo = require('./models/tv');

//config de db sequelize

const sequelize = new Sequelize('acamicadbtest', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

validar_con();

const Tv = tvModelo(sequelize, Sequelize);

sequelize.sync({ force:false})
    .then(() => {
        console.log('Tablas sincronizadas');
    });

async function validar_con() {
    try {
        await sequelize.authenticate();
        console.log('Conexión ok');
    } catch (error) {
        console.error('ERROR AL CONECTAR: ', error);
    }
}

module.exports = {
    Tv,
};