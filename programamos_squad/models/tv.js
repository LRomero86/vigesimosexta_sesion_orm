module.exports =  (sequelize, type) => {

    return sequelize.define('tv', {
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        
            marca: type.INTEGER,
            modelo: type.STRING,
            gama: type.STRING,
            pantalla: type.INTEGER,
            sistema_operativo: type.STRING,
            precio: type.NUMBER,        
    });
}

