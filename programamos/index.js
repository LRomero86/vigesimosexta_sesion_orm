const { Sequelize, DataTypes, Model } = require('sequelize');

//configuramos nuestra instancia de Sequelize para...

const sequelize = new Sequelize('acamicadb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

//verificamos que se conecte a la bd mysql

sequelize.authenticate()
    .then(() => {
        console.log("Conexión satisfactoria");
    })
    .catch(() => {
        console.log("Conexión con problemas");
    });

class Usuarios extends Model {}

Usuarios.init(
    {
        nombre: DataTypes.STRING,
        apellidos: DataTypes.STRING,
    },
    {
        sequelize,
        modelName: 'Usuarios',
        timestamps: false, //Para remover las columnas de createdAtm updatedAt
    }
);

class Casas extends Model {}

Casas.init(
    {
        nombre: DataTypes.STRING, //nombre de la casa
    },
    {
        sequelize,
        modelName: 'casas',
        timestamps: false,
    }
);

//asociación usando belongsTo
//si la llave foranea la va a tener la primera tabla usamos belongsTo
Usuarios.belongsTo(Casas,
    {
        foreignKey:'id_casa',
    }
);

//si la llave foranea la va a tener la segunda tabla se usa hasOne
//Usuarios.hasOne(Casas);

(async () => {
    //insertar datos
    await sequelize.sync({force:true});

    const datoPlaya = { nombre: 'Casa de la playa'};
    const dataInsertCasa = await Casas.create(datoPlaya);
    console.log(dataInsertCasa.toJSON());

    const datos = { nombre: 'Josefa', apellidos: 'Carolina', id_casa:1 };
    const usuarioData = await Usuarios.create(datos);

    //consultar, mostrará el último ingresado

    console.log(usuarioData.nombre);
    console.log(usuarioData.apellidos);

    //actualizamos sus valores

    usuarioData.nombre = 'Manuela';
    usuarioData.apellidos = 'Jimenez Update';

    //guardamos los cambios para actualizar
    await usuarioData.save();

    console.log(usuarioData.nombre); //Manuela
    console.log(usuarioData.apellidos);//Jimenez update

    //eliminamos el registro
    //await usuarioData.destroy();

    //buscar un solo resultado

    const userOne = await Usuarios.findOne({
        where: {
            nombre: 'Manuela',
        },
    });

    if (userOne === null) {
        console.log('Usuario no encontrado');
    } else {
        console.log('Usuario encontrado', userOne.toJSON());
    }

    //buscar varios
    //buscar muchos resultados

    const usuarios = await Usuarios.findAll({
        where: {
            nombre: 'Manuela',
        },
    });

    usuarios.forEach((item) => {
        console.log(item.toJSON()); //listo todos los resultados
    });


    let usuarioPlaya = await Usuarios.findAll({
        include: {
            model: Casas
        },
        attributes: ['nombre', 'apellidos']
    });

    usuarioPlaya.forEach((item) => {
        console.log('Datos playa', item.toJSON());
    });

})();